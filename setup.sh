#!/bin/bash

#core development environment
#     sudo ssh git emacs
#javascript development
#     phantomjs
#additional development dependencies
#     python2 base-devel (for compiling certain npm modules, like serialport
pacman -S --noconfirm sudo openssh emacs-nox git phantomjs python2 base-devel


#create new user
echo -n "Local user:"
read username < /dev/tty

x=0
while [ $x -eq 0 ]
do
    echo -n "Password:"
    read password < /dev/tty
    echo -n "Confirm password:"
    read passwordConfirmation < /dev/tty
    if [ $password = $passwordConfirmation ]
    then
        x=1
    else
        echo "Passwords do not match!"
    fi
done

echo "Creating user $username"
useradd -m -g users -s /bin/bash $username
echo "Setting password for $username"
echo "$username:$password"|/usr/bin/chpasswd

sudo $username <<'EOF'

#change to home directory
cd ~

#install nvm
curl https://raw.github.com/creationix/nvm/master/install.sh | sh

# Load nvm and install latest production node
source .nvm/nvm.sh
nvm install v0.10
nvm use v0.10
nvm alias deafult v0.10

#setup custom dotfiles
curl https://bitbucket.org/yianisn/archsetup/raw/master/.bash_aliases > ~/.bash_aliases

mkdir .emacs.d
curl https://bitbucket.org/yianisn/archsetup/raw/master/.emacs.d/init.el > ~/.emacs.d/init.el

#source aliases in .bash_profile
if [ -f ".bash_profile" ]; then
  PROFILE=".bash_profile"
fi

SOURCE_STR=" . ~/.bash_aliases"

if ! grep -qc '.bash_aliases' $PROFILE; then
  echo "=> Appending aliases source to $PROFILE"
  echo "" >> "$PROFILE"
  echo $SOURCE_STR >> "$PROFILE"
else
  echo "=> Aliases source already in $PROFILE"
fi

EOF


